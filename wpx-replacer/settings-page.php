<div class="wrap">
    <h1>مدیریت کلمات به لینک</h1>
    <style>
        .form-row{
            margin: 10px 0;
        }
    </style>
    <div class="settings-wrapper">
        <a style="display: block;margin: 10px 0;text-align: center;" class="add_new_item button button-primary" href="#">اضافه کردن آیتم جدید</a>
        <form action="" method="post" class="form_items">
                <div class="items">

                        <?php if(isset($replacers) && count($replacers) > 0): ?>
                            <?php foreach ($replacers as $keyword => $link): ?>
                                    <div class="form-row item">
                                <label for="">
                                    کلمه :
                                    <input type="text" name="source[]" value="<?php echo $keyword;  ?>">
                                </label>
                                <label for="">
                                    لینک :
                                    <input type="text" name="replace[]" style="direction: ltr;" value="<?php echo $link; ?>">
                                </label>
                                <a style="text-decoration: none;" class="remove_item" href=""><span class="dashicons dashicons-dismiss"></span></a>
                                    </div>
	                        <?php endforeach; ?>
                        <?php else: ?>
                    <div class="form-row item">
                            <label for="">
                                کلمه :
                                <input type="text" name="source[]">
                            </label>
                            <label for="">
                                لینک :
                                <input type="text" name="replace[]" style="direction: ltr;">
                            </label>
                            <a style="text-decoration: none;" class="remove_item" href=""><span class="dashicons dashicons-dismiss"></span></a>
                    </div>
                        <?php endif; ?>

                </div>
	        <?php submit_button('ذخیره تنظیمات'); ?>
        </form>

    </div>
</div>