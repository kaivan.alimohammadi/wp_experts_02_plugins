<?php
/*
Plugin Name: WPX Replacer
Plugin URI: http://7learn.com
Description: a plugin for replace words
Author: Kaivan Alimohammadi
Version: 1.0.0
Author URI: http://7learn.com
*/

define( 'WPX_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPX_URL', plugin_dir_url( __FILE__ ) );
define( 'WPX_JS_URL', WPX_URL . 'assets/js/' );

function wpx_replace_words( $content ) {
	$replacers = get_option( 'wpx_replacer_words', [] );
	if ( $replacers && count( $replacers ) > 0 ) {
		foreach ( $replacers as $keyword => $link ) {
			$content = preg_replace( '/' . $keyword . '/', '<a  href="' . $link . '">' . $keyword . '</a>', $content );
		}
	}

	return $content;
}

add_filter( 'the_content', 'wpx_replace_words' );

function wpx_activation() {
	update_option( 'wpx_general_settings', [
		'currency'   => '$',
		'baseWeight' => 10,
	] );
}

function wpx_deactivation() {

}

function wpx_uninstall() {
	delete_option( 'wpx_general_settings' );
}

register_activation_hook( __FILE__, 'wpx_activation' );
register_deactivation_hook( __FILE__, 'wpx_deactivation' );
register_uninstall_hook( __FILE__, 'wpx_uninstall' );

class WPX_Replacer {
	public function __construct() {
		add_action( 'admin_menu', [ $this, 'wpx_add_menu' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'register_assets' ] );
	}

	public function wpx_add_menu() {
		add_menu_page( 'کلمات به لینک', 'کلمات به لینک', 'manage_instagram', 'wpx-replacer', [ $this, 'wpx_main_page' ] );

	}

	public function wpx_main_page() {

		if ( isset( $_POST['submit'] ) ) {
			$replacers = [];
			$replaces  = $_POST['replace'];
			foreach ( $_POST['source'] as $index => $source ) {
				$replacers[ $source ] = $replaces[ $index ];
			}
			update_option( 'wpx_replacer_words', $replacers );
		}
		$replacers = get_option( 'wpx_replacer_words' );

		include WPX_DIR . 'settings-page.php';
	}

	public function register_assets() {
		wp_register_script( 'wpx_replacer_admin', WPX_JS_URL . 'admin.js' );
		wp_enqueue_script( 'wpx_replacer_admin' );
	}
}

new WPX_Replacer();
