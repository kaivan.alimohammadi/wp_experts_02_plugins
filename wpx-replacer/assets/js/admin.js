jQuery(document).ready(function ($) {
    $(document).on('click', '.add_new_item', function (event) {
        event.preventDefault();
        var frm = $('.form_items');
        var item = '            <div class="form-row item">\n' +
            '                <label for="">\n' +
            '                    کلمه :\n' +
            '                    <input type="text" name="source[]">\n' +
            '                </label>\n' +
            '                <label for="">\n' +
            '                    لینک :\n' +
            '                    <input type="text" name="replace[]">\n' +
            '                </label>\n' +
            '                <a style="text-decoration: none;" class="remove_item" href=""><span class="dashicons dashicons-dismiss"></span></a>\n' +
            '            </div>';
        frm.find('.items').append(item);

    });
    $(document).on('click', '.remove_item', function (event) {
        event.preventDefault();
        var parent = $(this).parent();
        parent.slideUp(200,function () {
            $(this).remove();
        });
    });
});