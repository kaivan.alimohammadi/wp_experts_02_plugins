<?php

namespace Application\Utility\Router;

class Router {

	public static function boot() {

		$currentAddress = self::getCurrentAddress();
		if ( self::isAddressDefined( $currentAddress ) ) {
			if ( ! is_user_logged_in() ) {
				wp_redirect( '/' );
				exit;
			}
			$target = self::getAddressTarget( $currentAddress );
			list( $handler, $method ) = explode( '@', $target );
			$handlerInstance = new $handler;
			$handlerInstance->$method();
			exit;
		}
	}

	public static function getCurrentAddress() {
		return strtok( $_SERVER['REQUEST_URI'], '?' );
	}

	public static function isAddressDefined( string $address ) {
		$addresses = include USER_PANEL_DIR . 'addresses.php';

		return array_key_exists( $address, $addresses );
	}

	public static function getAddressTarget( string $address ) {
		$addresses = include USER_PANEL_DIR . 'addresses.php';

		return $addresses[ $address ];
	}

}