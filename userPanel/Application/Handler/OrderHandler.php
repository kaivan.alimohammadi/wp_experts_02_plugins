<?php


namespace Application\Handler;


use Application\Handler\Base\Handler;

class OrderHandler extends Handler {
	public function __construct() {
		parent::__construct();
	}
	public function index() {

		global $wpdb,$table_prefix;
		$orders = $wpdb->get_results($wpdb->prepare("
					SELECT * 
					FROM {$table_prefix}user_orders
					WHERE order_user_id=%d
		",$this->currentUser->ID));
		$this->view('frontend.order.index',compact('orders'));

	}

}