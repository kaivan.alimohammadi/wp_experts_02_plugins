<?php

namespace Application\Handler\Base;

abstract class Handler {
	protected $currentUser;
	public function __construct() {
		$this->currentUser=  wp_get_current_user();
	}

	public function view( string $path, array $params = null ) {
		$rawPath       = $path;
		$convertedPath = str_replace( '.', DIRECTORY_SEPARATOR, $rawPath );
		$viewPath      = USER_PANEL_VIEWS . $convertedPath . '.php';
		if ( file_exists( $viewPath ) && is_readable( $viewPath ) ) {

			! is_null( $params ) ? extract( $params ) : null;
			include $viewPath;

			return true;
		}
		print "View File {$viewPath} Not Found.";
	}

	protected function isPostedForm( string $btnName = null ) {
		$btnName = ! is_null( $btnName ) ? $btnName : 'submit';

		return isset( $_POST[ $btnName ] );
	}

	protected function postInput( string $key ) {
		return isset($_POST[$key]) ? $_POST[$key] : null;
	}

}