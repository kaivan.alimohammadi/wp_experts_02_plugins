<?php

namespace Application\Handler;

use Application\Handler\Base\Handler;

class PanelHandler extends Handler {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$params = [
			'panel_title' => 'داشبورد'
		];
		$this->view('frontend.dashboard.index',compact('params'));
	}

}