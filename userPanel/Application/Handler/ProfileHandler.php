<?php


namespace Application\Handler;


use Application\Handler\Base\Handler;

class ProfileHandler extends Handler {

	public function index() {
		global $wpdb,$table_prefix;
		$currentUserID = get_current_user_id();
		if ( $this->isPostedForm() ) {
			$userData = [
				'ID'  => $currentUserID,
				'display_name' => $this->postInput( 'display_name' ),
				'user_email'   => $this->postInput( 'user_email' )
			];

			if ( isset( $_POST['user_password'] ) && ! empty( $_POST['user_password'] ) ) {
				if ( isset( $_POST['user_password_confirm'] ) && ! empty( $_POST['user_password_confirm'] ) ) {
					if ( $_POST['user_password'] == $_POST['user_password_confirm'] ) {
						$userData['user_pass'] = $this->postInput( 'user_password' );
					}
				}
			}
			$updatedUser = wp_update_user( $userData );
			if ( ! is_wp_error( $updatedUser ) ) {
				if ( isset( $_POST['user_mobile'] ) && ! empty( $_POST['user_mobile'] ) ) {
					update_user_meta( $updatedUser, 'user_mobile', $this->postInput( 'user_mobile' ) );
					wp_redirect('/panel/profile');
					exit;
				}
			}
		}
		$currentUser = wp_get_current_user();
		$userInfo = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$table_prefix}user_info WHERE user_id=%d",$currentUserID));
		$params      = [
			'panel_title' => 'پروفایل'
		];
		$this->view( 'frontend.profile.index', compact( 'params', 'currentUser','userInfo' ) );
	}

}