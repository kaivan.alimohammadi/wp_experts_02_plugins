<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>پنل کاربری</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.min.css">
	<style>
		body{
			padding: 50px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">منوی کاربری</h3>
				</div>
				<div class="panel-body">
					<ul class="list-group">
						<li class="list-group-item">
							<a href="/panel/profile">پروفایل کاربری</a>
						</li>
						<li class="list-group-item">
							<a href="/panel/orders">سفارش ها</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo  isset($params['panel_title']) ? $params['panel_title']: ''; ?></h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered table-stripe">
						<tr>
							<th>شناسه</th>
							<th>قیمت</th>
							<th>تاریخ ثبت</th>
							<th>وضعیت</th>
						</tr>
						<?php if(isset($orders) && count($orders) > 0): ?>
							<?php foreach ($orders as $order): ?>
								<tr>
									<td><?php echo $order->order_id; ?></td>
									<td><?php echo $order->order_total_price  ; ?></td>
									<td><?php echo $order->order_created_at; ?></td>
									<td><?php echo $order->order_status; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>