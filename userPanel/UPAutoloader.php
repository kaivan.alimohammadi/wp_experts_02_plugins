<?php


class UPAutoloader {
	public static function autoload( $class ) {
		$classParts = explode( '\\', $class );
		$className  = implode( '/', $classParts );
		$className  = USER_PANEL_DIR.DIRECTORY_SEPARATOR.$className.'.php';
		if ( file_exists( $className ) ) {
			include_once $className;
		}
	}
}