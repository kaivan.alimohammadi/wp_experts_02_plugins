<?php

/*
Plugin Name: 7learn User Panel
Plugin URI: http://7learn.com
Description: a plugin for replace words
Author: Kaivan Alimohammadi
Version: 1.0.0
Author URI: http://7learn.com
*/

class UserPanel {
	public function __construct() {
		register_activation_hook( __FILE__, [ $this, 'activation' ] );
		register_deactivation_hook( __FILE__, [ $this, 'deactivation' ] );
		add_action( 'init', [ $this, 'init' ] );
	}

	public function init() {
		$this->defineConstants();
		$this->autoloader();
		$this->bootRouter();
	}

	public function autoloader() {
		include "UPAutoloader.php";
		spl_autoload_register( 'UPAutoloader::autoload' );
	}

	public function activation() {

	}

	public function deactivation() {

	}

	public function defineConstants() {
		define( 'USER_PANEL_DIR', plugin_dir_path( __FILE__ ) );
		define( 'USER_PANEL_URL', plugin_dir_url( __FILE__ ) );
		define( 'USER_PANEL_VIEWS', USER_PANEL_DIR . 'views/' );
	}

	public function bootRouter() {
		\Application\Utility\Router\Router::boot();
	}

}

new UserPanel();