<div class="wrap">
    <h1>مدیریت دسترسی ها</h1>
    <form action="" method="post">
        <div class="wpx-acl-wrapper" style="padding: 50px;background: #FFFFFF;">
            <div style="margin: 20px 0">
				<?php if ( ! empty( $subscribers->get_results() ) ): ?>
                    <select name="user_id">
						<?php foreach ( $subscribers->get_results() as $subscriber ): ?>
                            <option value="<?php echo $subscriber->ID; ?>"><?php echo $subscriber->display_name; ?></option>
						<?php endforeach; ?>
                    </select>
				<?php endif; ?>
            </div>
			<?php if ( $caps && count( $caps ) > 0 ): ?>
				<?php foreach ( $caps as $name => $title ): ?>
                    <span>
                    <label class="ios7-switch">
                     <input
                             type="checkbox"
                             name="caps[]"
                             value="<?php echo $name; ?>"
                     >
            <span></span>
	                    <?php echo $title; ?>
        </label>
				</span>
				<?php endforeach; ?>
			<?php endif; ?>
            <div style="margin: 20px 0;">
                <button class="button-primary" name="save_acl">ذخیره تنظیمات</button>
            </div>
        </div>
    </form>
</div>